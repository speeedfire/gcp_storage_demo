<?php

namespace App\Http\Controllers;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Storage\StorageObject;

class GcpController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        //auth to bucket
        $storage = new StorageClient([
            'keyFile' => json_decode(file_get_contents(storage_path(env('GCP_AUTH_JSON'))), true)
        ]);

        $bucket = $storage->bucket(env('GCP_DEFAULT_BUCKET_NAME'));
        $bucket2 = $storage->bucket(env('GCP_DEFAULT_BACKUP_BUCKET_NAME'));

        $info = $bucket->info();

        echo 'Info:';
        dump($info);
        echo '<hr/>';
        // Upload a file to the bucket.

        echo 'upload:';
        $uploded = $bucket->upload(
            fopen(storage_path('app/logo.png'), 'r')
        );
        dump($uploded);
        echo '<hr/>';

        echo 'life:';
        $life = $bucket->currentLifecycle();
        dump($life);
        echo '<hr/>';

        $promises[] = $bucket->uploadAsync(
            fopen(storage_path('app/style.zip'), 'r')
        )->then(function (StorageObject $object) {
             dump($object->name());
         }, function(\Exception $e) {
             throw new Exception('An error has occurred in the matrix.', null, $e);
        });;

        foreach ($promises as $promise) {
             $promise->wait();
        }

        echo '<hr/>';

        // Download and store an object from the bucket locally.
        $logo = 'logo.png';
        $object = $bucket->object($logo);

        echo 'object:';
        dump($object);
        echo '<hr/>';

        $object->downloadToFile('/tmp/' . $logo);

        echo 'copy to another bucket';
        $object->copy($bucket2, ['name' => $object->name()]);
        printf('Copied gs://%s/%s to gs://%s/%s' . PHP_EOL,
            $bucket->name(), $object->name(), $bucket2->name(), $object->name());

        echo '<hr/>';

        echo 'delete:';
        $object->delete();
        printf('Deleted gs://%s/%s' . PHP_EOL, $bucket->name(), $logo);
        echo '<hr/>';

        $name = $bucket->name();

        echo 'name:';
        dump($name);
        echo '<hr/>';

        $list = $bucket->objects();

        echo 'objects name:';
        foreach ($list as $object) {
            dump($object->name());
        }
        echo '<hr/>';

        //list specific objects
        echo 'separated object list:';
        echo '<br/>';

        $options = ['prefix' => 'myDirectory/'];
        foreach ($bucket->objects($options) as $object) {
            printf('Object: %s' . PHP_EOL, $object->name());
            echo '<br/>';
        }
        echo '<hr/>';

    }
}
